package fp.daw.despliegue;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/postregistro")
public class PostregistroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String id = request.getParameter("id");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		if (id != null && email != null && password != null) {
			PrintWriter out = null;
			try {
				response.setCharacterEncoding("utf-8");
				out = response.getWriter();
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head>");
				out.println("<meta charset=\"UTF-8\" />");
				out.println("<title>USUARIO REGISTRADO</title>");
				out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/loginregistro.css\"");
				out.println(" media=\"screen\" />");
				out.println("<script type=\"text/javascript\" src=\"js/loginregistro.js\"></script>");
				out.println("</head>");
				out.println("<body>");
				out.println("<div id='bienvenida' class=\"form\">");
				out.println("<h1>BIENVENIDO " + id.toUpperCase() + "</h1>");
				out.println(
						"<img alt='imagen' id='imgBienv' src=\"https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/afbefb6e-a4d3-447e-bf7a-10d0fe1b8087/db9v2sl-526fde26-8c2d-4aa7-bb4f-67b22b13865c.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2FmYmVmYjZlLWE0ZDMtNDQ3ZS1iZjdhLTEwZDBmZTFiODA4N1wvZGI5djJzbC01MjZmZGUyNi04YzJkLTRhYTctYmI0Zi02N2IyMmIxMzg2NWMuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.6M9JCFMFkWZYk5vTNzse-eCBMd-RY8RB0BN8dVyNQFQ\">");
				out.println(
						"<h4>Se ha registrado correctamente y en breve recibir� un correo de confirmaci�n con un enlace en su direci�n "
								+ email + " para su activaci�n</h4>");
				out.println("</div>");
				out.println("</body>");
				out.println("</html>");

			} finally {
				if (out != null)
					out.close();
			}
		} else {
			response.sendRedirect("inicio");
		}
	}

}
