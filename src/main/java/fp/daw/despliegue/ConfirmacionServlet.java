package fp.daw.despliegue;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/confirmacion")
public class ConfirmacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource dataSource;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			InitialContext context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pf");
			if (dataSource == null)
				throw new ServletException("DataSource desconocido");
		} catch (NamingException e) {
			Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		doGet(request, response);
	}
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String cc = request.getParameter("cc");
		if (cc != null) {
			try {
				enviarRespuesta(response, confirmar(cc));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			response.sendRedirect("inicio");
		}
	}
	
	private Boolean confirmar(String cc) throws SQLException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String sql = null;
		connection = dataSource.getConnection();
		statement = connection.createStatement();
		sql = String.format("select * from usuarios where id='%s'", cc);
		resultSet = statement.executeQuery(sql);
		if (resultSet.next()) {
			return false;
		} else {
			return true;
		}

	}
	
	private void enviarRespuesta(HttpServletResponse response, Boolean boolean1) throws IOException {
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<meta charset=\"UTF-8\" />");
			out.println("<title>USUARIO CONFIRMADO</title>");
			out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/loginregistro.css\"");
			out.println(" media=\"screen\" />");
			out.println("<script type=\"text/javascript\" src=\"js/loginregistro.js\"></script>");
			out.println("</head>");
			out.println("<body>");
			out.println("<div id='bienvenida' class=\"form\">");
			out.println("<h1>USUARIO CONFIRMADO CORRECTAMENTE</h1>");
			out.println("<img alt='imagen' id='imgIni' src=\"http://i.imgur.com/LZPt8k3.jpg\">");
			out.println("</div>");
			out.println("</body>");
			out.println("</html>");

		} finally {
			if (out != null)
				out.close();
		}
	}

}
